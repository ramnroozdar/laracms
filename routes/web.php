<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index');
Route::group(['prefix' => 'admin' , 'namespace' => 'Admin'], function () {
    Route::get('/','AdminController@index')->name('admin');
    Route::get('users','UsersControllers@index')->name('admin.users');
    Route::get('users/create','UsersControllers@create')->name('admin.users.create');
    Route::post('users/store','UsersControllers@store')->name('admin.users.store');
    Route::get('users/delete/{user_id}','UsersControllers@delete')->name('admin.users.delete');
    Route::get('users/edit/{user_id}','UsersControllers@edit')->name('admin.users.edit');
    Route::post('users/update/{user_id}','UsersControllers@update')->name('admin.users.update');
});


