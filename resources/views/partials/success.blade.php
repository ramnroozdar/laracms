@if (session('status'))
    <div class="alert alert-success text-white">
        {{ session('status') }}
    </div>
@endif