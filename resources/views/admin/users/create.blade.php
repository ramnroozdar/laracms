@extends('layouts.admin')

@section('content')
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-title">
                        <h4>ثبت نام کاربر جدید</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="basic-form p-10">
                                    @include('partials.errors')
                                    @include('partials.success')
                                    <form method="post" action="{{ route('admin.users.store') }}">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label for="userFullName">نام و نام خانوادگی</label>
                                            <input id="userFullName" name="userFullName" type="text"
                                                   class="form-control input-default hasPersianPlaceHolder" value="{{ old('userFullName') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="userEmail">آدرس ایمیل</label>
                                            <input id="userEmail" name="userEmail" type="email"
                                                   class="form-control input-default hasPersianPlaceHolder" value="{{ old('userEmail') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="userPassword">رمز عبور</label>
                                            <input id="userPassword" name="userPassword" type="password"
                                                   class="form-control input-default hasPersianPlaceHolder" value="{{ old('userPassword') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="userRole">نقش کاربری :</label>
                                            <select id="userRole" class="form-control persianText">
                                               @foreach($userRoles as $roleID => $roleTitle)
                                                    <option value="{{ $roleID }}">{{ $roleTitle }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group m-t-20">
                                            <input type="submit" class="btn btn-primary m-b-10 m-l-5" value="ثبت اطلاعات">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop