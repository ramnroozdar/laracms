<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $users = ['a','b','c'];
        return view('welcome',compact('users'));
    }
}
